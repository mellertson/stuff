# V12 Market Opportunity

The market opportunity from V12 is created by two distinct product offerings:
    1. Voting System Services offered to all U.S. voting districts.
    2. Payment Processing Services offered to all U.S. citizens and companies.

All revenue garnered by both market opportunities described here, are shared with all V12 node operators.

In order to have sufficient computer processing power needed for V12 to offer both services 42,000 V12 Witness Nodes must be provisioned throughout the nation.  The 42,000 V12 Witness Nodes must be:

* provisioned in a decentralized manner, meaning randomly distributed throughout households in the United States.
* owned and operated ONLY by valid U.S. citizens.

Operators of V12 Witness Nodes will be compensated as follows:

* An average transaction on the V12 network is 100 bytes in size.
* The processing fee for an average V12 transaction is approximately $0.16 dollars (16 cents per 100 bytes).
* All transaction fees will be equally distributed to all V12 node operators, with a node connected to the V12 network at the time a transaction was processed.

## V12 Market Opportunity: Voting System Services

Presently, $75 million dollars per year is awarded by U.S. States and Voting Districts to companies like Dominion Voting and other competitors in exchange for "Voting Services".  Assuming 42,000 V12 Witness Nodes are operational, then even if we garner 100% of the market for Voting Services the maximum opportunity for each V12 node operator would be $1,785.00 per year.  Which is enough to recover the cost of purchasing a V12 Witness Node in year 1.  But, it is not a significant financial opportunity.  

However, it is imperative that V12 Voting Services be utilized by a majority of citizens of the United States.  Because, historically it is clear that if a solution is presented which threatens the base of power of the top 1%, then regulators and politicians can and have moved swiftly to shutdown innovators and creators who bring forth solutions for the common good.  And law enacted by corrupt politicians has been the weapon of choice for hostile actors, both foreign and domestic, to stifle and oppress We The People.  V12 is necessary to prove fraud to the U.S. Supreme Court, so that hostile actors do not have the opportunity to weaponize U.S. law against her citizens.  V12 Voting Services are necessary in order to protect and defend the U.S. Constitution, the rights of We The People, and also the revenue stream created for the 42,000 V12 node operators, which will be the instrument of injecting over $8 billion dollars a year back into the U.S. economy.  And, as you will see in the following section, V12 simultaneously and permanently eliminates a hidden tax of $97 billion dollars a year from We The People.  And will at the same time drain the afore mentioned $97 billion dollars from corrupt corporations, thus weakening their ability to spread political corruption through lobbyist organizations, bribes, and kick backs.  

The Vet The Vote (V12) network does this while honoring all U.S. laws, the rights of each individual, and without the need for riots nor violence of any kind.  V12 is the key to restoring sanity, and prosperity, to our nation.

## V12 Market Opportunity: Financial Ledger Services

According to the [U.S. Payments Study of 2022][uspayments2022] (Reference #1), conducted by the U.S. Federal Reserve, and also according to the [Visa USA Interchange Reimbursement Fees][visafees] (Reference #2), published by Visa, the following revenues are presently garnered by U.S. Banks and other "Money Transmitters".  Additionally, in accordance with U.S. Federal Regulations, set by the U.S Department of Treasure, and enforced by FinCEN, any financial institution offering "Money Transmitter Services" must obtain a Money Transmitter License from each of the 50 U.S. States.  A single Money Transmitter License can cost in excess of 1 million dollars, so in order to offer Money Transmitter Services in all 50 U.S. States a company must pay over $50 million dollars, which is out of the reach of the vast majority of U.S. citizens and companies.

However, V12 Node Operators cannot be classified as "Money Transmitters".  Because, according to [U.S. Legal Code 2022 Title31, Subtitle IV, Chapter 53, Sub-chapter II, Section 5330, Sub-section d][ussection5330] (page 479, column 2, starting with "(2) MONEY TRANSMITTING SERVICE") (Reference #3), a "Money Transmitter Service" must first accept currency or funds and then transmit the accepted currency or funds via a network.  However, V12 enables private individuals to transmit the funds they own and are in possession of themselves directly to another private individual or company.  The V12 network simply provides the compute resources, which prove that the private individual transmitted currency or funds.  In order words, each of the 42,000 V12 Witness Nodes act as a "witness" of private individuals transmitting currency or money.  This is why the a V12 node is called "Witness Node".  Additionally, this is the same reasoning applied to Bitcoin, which is why, according to Investopedia, the [SEC has not classified Bitcoin as a "security"][btcclass] (Reference #4).

Presently, banks and other money transmitters garner annual fee revenue of $97.9 billion dollars per year.  For the curious, this is calculated as: `$97.9 billion dollars = ($3.14 trillion transactions * 0.0295 Percentage Fee) + (52.4 billion transactions * $0.10 Flat Fee)`.  To simplify the calculation, it breaks down into the following two revenue streams.

1. Annual Revenue from a $0.10 Flat Fee for each transaction is $5.24 billion dollars per year, which is calculated as: `$5.24 billion dollars = 52.4 billion transactions * $0.10 Flat Fee`.
2. Annual Revenue from a 3% Percentage Fee on the amount of each transaction is $92.6 billion dollars per year, which is calculated as: `$92.6 billion dollars = $3.14 trillion dollars worth of transactions * 0.0295 Percentage Fee`.

V12 replaces both the Flat Fee and Percentage Fee with a more simple, and reasonable, fee of approximately $0.16 per transaction.  Based on an average fee of 16 cents per transaction, the total market opportunity created by V12 is $8.4 billion dollars per year.  Which, is calculated as: `$8.4 billion dollars = 52.4 billion transactions * $0.16 dollars per transaction`.

Because, the fee revenue generated by V12 is equally distributed among the 42,000 node operators, the opportunity for each node operator is $200,000 dollars per year.  Which, is calculated as: `$200,000 dollars = $8.4 billion dollars in transaction fees / 42,000 node operators`.  It should also be noted, that the estimates revenue estimates described here are based solely on financial transactions, but the V12 network will be capable of processing other types of transactions, including but not limited to:

* encrypted text messages
* social media posts
* phone calls
* and any other kind of transaction presently handled by the likes of Twitter, Amazon, Google, Apple, Facebook, and any other platform.

If V12 is adopted for other types of transactions, like text messages, social medial posts, phone calls, and the like it could double or triple the amount of data processed by the V12 network.  And if the amount of data doubles or triples, then the fee revenue doubles or triples.  

The sky is literally the limit.  Our proposal described here is simply this, focus on what matters most and help our nation Vet The Vote of the 2024 Presidential Election, and you will also be ensuring the financial future of your family, city, state, and country at the same time.  While restoring sanity and freedom and showing the corrupt big tech companies and politicians alike that We The People are indeed the powerhouse, and heart, of America.

But, with election day, November 5th, 2024, only 11 months away time is of the essence, so we respectfully ask that you take action by purchasing a limited edition V12 Witness Node today.

### Clarification on "Approximate" Transaction Size

We say a transaction on the V12 network is "approximately" 100 bytes, because remember that the V12 network simply provides "compute resources" to private individuals.  The fee for the V12 compute resources is based on the amount of data processed and stored on the V12 blockchain, because it takes more computing power to encrypt, process, and store a larger transaction.  The average size of a transaction on the V12 network is 100 bytes, but it can vary depending on how an individual uses the V12 network.  For example, if an individual uses V12 for a financial transaction, then the size of the transaction will be 100 bytes.  But, if an individual uses V12 to send an encrypted text message, then the size of the transaction might be 200, 300, or even 1,000 bytes.  In the latter case, the size of the transaction is determined by the amount of text contained in the encrypted message.  Please note, that for all revenue estimates provided in this section we assume a transaction size of 100 bytes.



# Payments Data

The following information is based on the [**U.S. Federal Reserve Payments Study of 2022**][uspayments2022] (Table 1. Noncash payments, 2015, 2018, and 2021) (Reference #1).

* **Revenue from debit + credit card payments:** 
    - $9.43 trillion dollars from 2019-2021 (3 year revenue)
    - $7.08 trillion dollars from 2016-2018 (3 year revenue)
    - $5.52 trillion dollars from 2013-2015 (3 year revenue)
    - Average revenue per year: $2.48 trillion dollars per year from 2013-2021 (9 years)
    - Average revenue per year: $3.14 trillion dollars per year from 2018-2021 (3 years)
* **Number of transactions from debit + credit card payments**:
    - 157.0 billion transactions from 2019-2021 (3 year total)
    - 131.2 billion transactions from 2016-2018 (3 year total)
    - 101.5 billion transactions from 2013-2015 (3 year total)
    - Average transactions per year: 43.3 billion transactions per year from 2013-2021 (9 years)
    - Average transactions per year: 52.4 billion transactions per year from 2018-2021 (3 years)

* Revenue streams of "licensed money transmitters":
    - $0.10 Flat Fee per transaction (flat fee per transaction, e.g. $100 purchase = $0.10 fee)
    - 3.0% Percentage Fee of transaction volume (amount of each transaction, e.g. $100 purchase = $3.00 fee)
    - For supporting reference material see: [Visa USA Interchange Reimbursement Fees, page 11][visafees] (Reference #2)


# References

1. U.S. Federal Reserve Payments Study of 2022: https://www.federalreserve.gov/paymentsystems/fr-payments-study.htm
2. Visa USA Interchange Reimbursement Fees: https://usa.visa.com/content/dam/VCOM/download/merchants/visa-usa-interchange-reimbursement-fees.pdf
3. U.S. Legal Code 2022 Title31, Subtitle IV, Chapter 53, Sub-chapter II, Section 5330, Sub-section d: https://www.govinfo.gov/content/pkg/USCODE-2022-title31/pdf/USCODE-2022-title31-subtitleIV-chap53-subchapII-sec5330.pdf
4. Investopedia, SEC Crypto Regulations: What Financial Advisors Need to Know: https://www.investopedia.com/crypto-regulations-for-financial-advisors-8402046

[uspayments2022]: https://www.federalreserve.gov/paymentsystems/fr-payments-study.htm
[visafees]: https://usa.visa.com/content/dam/VCOM/download/merchants/visa-usa-interchange-reimbursement-fees.pdf
[ussection5330]: https://www.govinfo.gov/content/pkg/USCODE-2022-title31/pdf/USCODE-2022-title31-subtitleIV-chap53-subchapII-sec5330.pdf
[btcclass]: https://www.investopedia.com/crypto-regulations-for-financial-advisors-8402046
