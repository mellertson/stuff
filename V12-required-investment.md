```
NOTE: The information in this page on the topics of required seed capital and also capital allocation
is now outdated.  Please see the page titled `Company Synopsis` for current seed capital requirements.
```

## Required Investment Capital

An investment of 1.25 million dollars is being sought in order to market the 
sale of 42,000 computers to the American public.  And in order to acquire a 75% 
stake in existing gold-backed crypto-currency company, name Quintric.  Members 
of the Repulican Party have committed to invest an additional 1.5 million 
dollars in Quintric's gold-backed tokens, to offer as a reward to American 
citizens who volunteer their time to act as "poll watchers".  Quintric's 
gold-backed tokens have already been fully vetted by the Securities and Exchange 
Commission (SEC).

42,000 computers are required to ensure the Vet The Vote system has enough 
processing power to handle the volume of transactions, which must be fully 
vetted in a timely fashion on election day.  To better understand this requirement 
consider the following.


# Deep Dive on Vet The Vote

## Estimating the number of votes in the 2024 elections

According the U.S. Census data from 2020 elections...
* 168 million people registered to vote
* 155 million people voted

Assuming at least 155 million people will vote in the 2024 elections, then the 
winning candidate must garner at least 1/2 of those, or 77.5 million votes.  
But, we can assume voters from Democratic, Republican, and other political 
parties will use Vet The Vote, so to error on the side of caution, and ensure 
the system has adequate processing power, we add an additional 60% to our 
estimates.  Meaning we estimate the number of votes the system will need to 
process on election day will be 125 million.

**Estimated users of Vet The Vote:** 125 million

## Estimating the required computer resources

In order for the aggregated results of the Vet The Vote system to be deemed 
"evidentiary" in a court of law, it is critical it follows current US legal 
process.  And also for Vet The Vote's processes to be independantly verifiable.  
Case in point, on August 8, 2021 the U.S. District Court of Colorado ordered 
sanctions for Plaintifs to pay Defendants legal fees in a 
[civil case][usdc-colorado-order] (reference #3) against Dominion Voting Systems, Facebook, 
Mark Zuckerberg, and others including "various state officials (including 
governors) from the states of Georgia, Michigan, Pennsylvania, and Wisconsin".  
On page 65 of the court's order it reads, "Plaintiffs’ counsel did not conduct 
a reasonable inquiry into whether the factual contentions had 
**evidentiary support**" (bold added).  Additionally, Plaintifs claimed they 
represented "160 million Americans" in their class action lawsuit.  However, 
160 million Americans did not authorize the lawsuit.

It is commonly known that when a criminal case is tried in a U.S. court 12 
jurors are randomly selected.  In accordance with U.S. law Vet The Vote requires 
12 randomly selected citizens to act as "poll watchers" when verifying each of 
the 125 million votes.  In order to ensure that 12 citizens participate as poll 
watchers, 20 are randomly selected.  A vote is considered "verified" when at 
least 12 certify a ballot.  And one transaction must be processed by the Vet 
The Vote system for each of the 20 verifications.  So, if all 20 poll watchers 
verify a vote, the system needs to process 125 million ballots, up to 20 times 
each.  In other words, it must be capable of processing 2.5 billion transactions 
in less than 24 hours.  Which is about 29,000 transactions/second.

For a better understanding of the difficulty involved in processing transactions 
at this scale, consider Visa, Master Card, American Express, and other credit 
and debit card processors, who process more transactions than any other systems 
in the world.  All of them combined process 4,100 transactions/second.  So this 
means in Vet The Vote must be 7 times faster than all debit and credit 
processors combined.  While this might sound like an impossible challenge, it 
is actually quiet simple to overcome.

A new technology ermerged in 2009 known as blockchain, but is more commonly 
known as Bitcoin.  Bitcoin is nothing more than a world-wide super computer 
where many computers act as one.  Since 2009 many other blockchains have 
emerged, each with varying strengths and capabilities.  And while Bitcoin 
processes financial transactions, blockchains can process any type of 
transaction imaginable, and can do anything a regular computer can do, but can 
do it at a world-wide scale.  The most scalable blockchain to date, EOS, was 
created in 2018.  The scaling capability of EOS has been shown to sustain 4,000 
transactions/second.  We have cloned EOS and repurposed it into the Vet The Vote 
(V¹²) system.

So, to verify 29,000 votes/second we would need 8 V¹² networks operating in 
unison.  And the number of computer servers (nodes) required to run a single 
V¹² network varies.  For example, some experts estimate there are 1 million 
nodes currently running the Bitcoin network.  Whereas other networks run as few 
as 2,000 nodes.  The general rule of thumb is simply this, the more nodes there 
are, the more the network can be trusted.  Based on analysis of many of the 
largest blockchains more than 5,250 nodes are required for each V¹² network, or 
42,000 nodes in total.


# Capital Allocation

The $2.5M in investment capital will be allocated as follows for year 1 
of company startup.

* 40% (1 M) manufacturing
* 20% (500 K) acquisition of 51% ownership stake in Quintric
* 10% (250 K) operational expenses (including remaining R&D)
* 30% (750 K) sales and marketing


# Research and Development

Approximately 98% of all required software has already been developed and proven at scale under real world conditions.  The remaining 2% of R&D effort will be expended on improvements to the mobile app user interface, which is trivial by comparison to making it functional. For this reason, most R&D expenses have been eliminated.  And thus the required investment capital has been significantly reduced.


# Revenue Projections

If target sales of 42,000 V¹² nodes is achieved, then gross revenue of $63 
million is projected over the next 12 calendar months of year 1.  With a margin 
of 33%, NET revenue of $21 million is projected over the same 12 calendar months 
of year 1.

In years 2 and 3 gross revenue of $30 million per year is projected from the 
sale of election services to the 50 U.S. states.  The projections of $30 million 
per year are based on the actual revenues of competitors.  According to 
[Forbes, Dominion Voting Systems][dominion-forbes] (reference #6) alone earned revenue of 
$118.3 million over a 3 year period.

**Table 1: Revenue Projections**

| Product or Services       | Year 1 | Year 2 | Year 3 | Total |
| ------------------        | ------ | ------ | ------ | ----- |
| Voting Service            |   $21M |    $5M |   $10M |  $36M |
| Quintric Transaction Fees |   $??M |    $?M |   $??M |  $??M |



# 24 Month Operational Budget

From the $21 million in projected NET revenue, the following budget is 
established to continue operations, and to provide customer support, over 24 
calendar months (years 2-3).  In order to be conservative no revenue from years 
2 or 3 have been included in the following budget.

| Budget | Year 1 | Year 2 | Total |
| ------ | ------ | ------ | ----- |
| Sales and Marketing | $ 2.1M (10%) | $ 2.1M (10%) | $ 5M (20%) |
| Operations and Support | $ 2.1 million (10%) | $ 2.1 million (10%) | $ 5M (20%) |
| Total | | | $10M (40%) |


# Return on Investment


# Exit Strategy





## V¹² Node Specs and Cost

Each V¹² node must have the following computing hardware at minimum.

* CPU: AMD Ryzen 9 5950X
* RAM: 64 GB
* DISK: 2 x 4TB SSDs in RAID 0 (striped for better performance)
* NETWORK, either:
	- Wired Ethernet adapter OR
	- WiFi adapter
* GPU: None (it will be a "headless" system)

Several computer hardware manufacturers have provided quotes on the required 
compute hardware.  And also to provide hardware and support services for a 
period of 2 years.  Based on those quotes V¹² nodes can be purchased for $1,000 
each.  And sold to the general public for $1,500 each, yielding revenue of $500 
per V¹² node at a 33% margin.

* MainGear.com (web-form)
* OriginPC.com (email)
* Falcon-NW.com (email)
* DigitalStorm.com (phone, left inquiry with receptionist)



# Project Source Code

All source code for the project can be found here 
https://cybertron.ninja/bitbucket/projects/V12.  The source code includes the 
following components:

* V¹² Mobile App
* V¹² Vyper smart contracts
* V¹² Node



# References

1. US Census: https://www.census.gov/content/dam/Census/library/publications/2022/demo/p20-585.pdf
2. Faithless Votes: https://en.wikipedia.org/wiki/Faithless_elector
3. U.S. District Court - Colorado Order: http://cdn.cnn.com/cnn/2021/images/08/04/coloradotrumplawyers.pdf
4. U.S. Federal Reserve Payments Study of 2019: https://www.federalreserve.gov/newsevents/pressreleases/files/2019-payments-study-20191219.pdf
5. EOS Blockchain Scalability: https://www.blockchain-council.org/cryptocurrency/top-cryptocurrencies-with-their-high-transaction-speeds/
6. Dominion Voting by Forbes: https://www.forbes.com/sites/adamandrzejewski/2020/12/08/dominion-voting-systems-received-120-million-from-19-states-and-133-local-governments-to-provide-election-services-2017-2019/?sh=49df309c620f

[us-census]: https://www.census.gov/content/dam/Census/library/publications/2022/demo/p20-585.pdf
[faithless-votes]: https://en.wikipedia.org/wiki/Faithless_elector
[usdc-colorado-order]: http://cdn.cnn.com/cnn/2021/images/08/04/coloradotrumplawyers.pdf
[fed-payments-study]: https://www.federalreserve.gov/newsevents/pressreleases/files/2019-payments-study-20191219.pdf
[eos-scalability]: https://www.blockchain-council.org/cryptocurrency/top-cryptocurrencies-with-their-high-transaction-speeds/
[dominion-forbes]: https://www.forbes.com/sites/adamandrzejewski/2020/12/08/dominion-voting-systems-received-120-million-from-19-states-and-133-local-governments-to-provide-election-services-2017-2019/?sh=49df309c620f


